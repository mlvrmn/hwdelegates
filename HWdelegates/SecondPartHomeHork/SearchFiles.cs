﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HWdelegates
{
    public class Searcher
    {
        public void SearchFiles(String path)
        {
             Boolean pathIsExist =  Directory.Exists(path);
            if (pathIsExist)
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                FileInfo[] fileInfos = directoryInfo.GetFiles();

                if (fileInfos != null)
                {
                    FileArgs fileArgs = new FileArgs();

                    foreach (FileInfo info in fileInfos)
                    {
                        fileArgs.FileName = info.Name;

                        FileFound(info, fileArgs);
                    }
                }
            }
            else { Console.WriteLine("Каталога не существует"); };
        }

        public event EventHandler<FileArgs> FileIsFound;
        private void FileFound(Object sender, FileArgs fileArgs)
        {
            FileIsFound?.Invoke(sender, fileArgs);
        }
    }
}
