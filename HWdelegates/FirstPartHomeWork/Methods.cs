﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWdelegates
{
    public class Methods
    {
        static public List<TestModule> GetTestModules(Int32 countModules)
        {
            List<TestModule> testModules = new List<TestModule>();

            Int32 iteration = 0;

            while (iteration != countModules)
            {
                testModules.Add(new TestModule($"Элемент № {iteration}", iteration.ToString()));
                iteration++;
            }
            return testModules;
        }

        static public Func<TestModule, Single> GetParametr = (testModule) =>
           {
               Single result = 0;
               Boolean parseSuccess = Single.TryParse(testModule.Value, out result);
               if (!parseSuccess) return 0;
               else return result;
           };
    }
}
