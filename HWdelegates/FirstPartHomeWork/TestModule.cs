﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWdelegates
{
    public class TestModule
    {
        public String Name { get; set; }
        public String Value { get; set; }

        public TestModule(String name, String value)
        {
            Name = name;
            Value = value;
        }
    }
}
