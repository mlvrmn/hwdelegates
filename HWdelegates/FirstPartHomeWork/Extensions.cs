﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWdelegates
{
    public static class Extensions
    {
        public static T GetMax<T>(this IEnumerable<T> collection, Func<T, Single> GetParametr) where T : class
        {
            Single maxValue = 0; 

            T obj = null;

            foreach (var element in collection)
            {
                Single parametr = GetParametr(element);
                if (parametr > maxValue)
                {
                    maxValue = parametr;
                    obj = element;
                }
            }

            return obj;
        }
    }
}
