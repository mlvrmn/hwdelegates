﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HWdelegates
{
    class Program
    {

        Searcher searcher = new Searcher();

        public void FileIsFound_Handler(Object sender, FileArgs fileName)
        {
           Console.WriteLine(fileName.FileName);
        }

        public Program()
        {
            searcher.FileIsFound += FileIsFound_Handler; 
        }

        static void Main(string[] args)
        {
            /////////////////////1-я часть дз/////////////////////////////////////////
            Methods methods = new Methods();

            List<TestModule> testModules = Methods.GetTestModules(100);

            TestModule maxElement = testModules.GetMax(Methods.GetParametr);

            Console.WriteLine($"Максимальный элемент коллекции{maxElement.Name}");

            Console.ReadKey();


            //////////////////////2-я часть дз////////////////////////////////////////

            Program program = new Program();

            Console.WriteLine("Введите имя каталога, в котором будет происходить поиск файлов");
            String str = Console.ReadLine();
            
            program.searcher.SearchFiles(str);

            Console.ReadKey();
        }
    }
}
